import 'reflect-metadata'
import { DataSource } from 'typeorm'
import { User } from './entity/User'

export const AppDataSource = new DataSource({
  type: 'mysql',
  host: 'localhost',
  port: 9906,
  username: 'baluong',
  password: '123456',
  database: 'emba',
  synchronize: true,
  logging: false,
  entities: [User],
  migrations: [],
  subscribers: []
})
